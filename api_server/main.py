#!/bin/python3

import bottle
import json

app = application = bottle.Bottle()

debug = False
host = "0.0.0.0"
port = 8080

class EnableCors(object):
    name = 'enable_cors'
    api = 2

    def apply(self, fn, context):
        def _enable_cors(*args, **kwargs):
            # set CORS headers
            bottle.response.headers['Access-Control-Allow-Origin'] = 'http://cloudacademy.com'
            bottle.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
            bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, X-SessionID'

            if bottle.request.method != 'OPTIONS':
                # actual request; reply with the actual response
                return fn(*args, **kwargs)

        return _enable_cors

# @app.route('/', method=['OPTIONS', 'GET'])
@app.route('/hello', method=['OPTIONS', 'GET'])
def hello():
	bottle.response.set_header('Content-Type', 'application/json')
	return json.dumps({"result":"hi"})

app.install(EnableCors())

if __name__ == '__main__':
	#app.run(host=host, port=port, debug=debug)
	app.run(server='auto',host=host,port=port,reloader=True,debug=debug)
